@extends ('layouts.page')
@section ('title', __('ads.edit'))

@section ('content')
	<script src="/js/autocomplete_currencies.js"></script>
	<script src="/js/autocomplete_cities.js"></script>
	<script type='text/javascript'>
		let current_ad = {{ $ad -> id }};
		let Ad = {
			Images: {
				Remove: (id) => {
					let t = '#image-'+id;
					let obj = $ (t) [0];
					let cf = confirm ('{{ __ ('messages.confirmImageDeletion') }}');
					if (!cf)
						return;

					obj.remove ();
					$.ajax ({
						url: '{{ route ('ad.deleteImage') }}',
						method: 'post',
						data: {
							ad_id: current_ad,
							image_id: id,
							_token: '{{ csrf_token() }}',
						},
					})
				}
			}
		}
	</script>
	<div class="offset-md-2 col-md-8">
	<a href="{{ route ('ad', $ad->id) }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>  {{ __ ('ads.backToView') }}</a>
	<div class="display-4 mb-3">{{ __ ('ads.edit') }}</div>
		<form action="{{ route ('ad.pedit') }}" METHOD="POST" class="form">
			@csrf
			<input type="hidden" name='id' value='{{ $ad->id }}'>
			<div class="form-group">
				<label for="title">{{ __ ('common.title') }}</label>
				<input value="{{ $ad->title }}" type="text" class="form-control" name="title">
				@if ($errors->has('title'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
           		@endif
			</div>
			<div class="form-group">
				<label for="description">{{ __ ('common.description') }}</label>
				<textarea style="min-height: 100px" spellcheck="false" name="description" class="form-control" cols="30" rows="5">{{ $ad->description }}</textarea>
				@if ($errors->has('description'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
           		@endif
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-9">
						<label for="price">{{ __ ('common.price') }}</label>
						<input value="{{ $ad->price }}" type="text" class="form-control" name="price">
						@if ($errors->has('price'))
		                    <span class="invalid-feedback d-block" role="alert">
		                        <strong>{{ $errors->first('price') }}</strong>
		                    </span>
		           		@endif
					</div>

					<div class="col-md-3">
						<label for="currency">{{ __ ('common.currency') }}</label>
						<input value="{{ $ad->currency }}" id="currencies" type="text" class="form-control" name="currency">
						@if ($errors->has('currency'))
		                    <span class="invalid-feedback d-block" role="alert">
		                        <strong>{{ $errors->first('currency') }}</strong>
		                    </span>
		           		@endif
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="city">{{ __ ('common.city') }}</label>
				<input value="{{ $cityname }}" id="input-query"" type="text" class="form-control" name="city">
				@if ($errors->has('city'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
           		@endif
			</div>


			<label for="">{{ __ ('common.images') }}</label>
			<div class="form-group background-white p-4">
				<a href="{{ route ('ad.uploadImageForm', $ad->id) }}" class="btn bg-success text-white">
					<i class="fa fa-plus"></i>
				</a>
				<div class="pl-3 d-inline-block">
					@foreach ($ad -> images as $image)
					<a id="image-{{ $image -> id }}" class="pointer d-inline-block" onclick="Ad.Images.Remove ({{ $image -> id }})">
						<div class="position-relative d-block">
							<img class="uploaded-image rounded" src="{{ $image -> url }}" alt="{{ $image -> alt }}">
							<div class="hoverToDelete rounded"><i class="fa fa-trash"></i></div>
						</div>
					</a>
					@endforeach
				</div>
			</div>

			<div class="form-group">
				<label for="captcha">{{ __ ('auth.captcha') }}</label>
				<div class="p-3">@captcha</div>
				<input type="text" class="form-control" name="captcha">
				@if ($errors->has('captcha'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('captcha') }}</strong>
                    </span>
           		@endif
			</div>

			<input type="submit" class="mb-5 btn btn-primary" value="{{ __ ('common.submit') }}">
		</form>
	</div>
@endsection