@extends ('layouts.page')
@section ('title', __ ('ads.uploadImage'))

@section ('content')
<div class="row">
	<div class="col-md-6 offset-md-3">
		<div class="display-4">{{ __('ads.uploadImage') }}</div>
		<form action="{{ route('ad.uploadImagePost', $ad -> id) }}" class="form" enctype="multipart/form-data" method="POST">
			@csrf
			<input type="hidden" name="id" value="{{ $ad -> id }}">
			<div class="form-group">
				<label for="image">
					{{ __('user.labelAvatar') }}
					@if ($errors->has('image')) 
						<p class="text-danger">
							{{ $errors->first('image') }}
						</p>
					@endif
				</label>
				<input type="file" name="image" class="form-control w-100">
			</div>

			<div class="form-group">
				<label for="captcha">
					{{ __('auth.captcha') }}
					@if ($errors->has('captcha')) 
						<span class="text-danger">
							{{ $errors->first('captcha') }}
						</span>
					@endif
				</label>
				<div class="p-3">@captcha</div>
				<input type="text" name="captcha" class="form-control w-100">
			</div>
			<input type="submit" value="{{ __('common.submit') }}" class="btn btn-primary">
		</form>
	</div>
</div>
@endsection