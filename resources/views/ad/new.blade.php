@extends ('layouts.page')
@section ('title', __('ads.addNew'))

@section ('content')
	<script src="/js/autocomplete_currencies.js"></script>
	<script src="/js/autocomplete_cities.js"></script>
	<div class="offset-md-2 col-md-8">
	<div class="display-4 mb-5">{{ __ ('ads.addNew') }}</div>
		<form action="{{ route ('ad.pcreate') }}" METHOD="POST" class="form">
			@csrf
			<div class="form-group">
				<label for="title">{{ __ ('common.title') }}</label>
				<input value="{{ old ('title') }}" type="text" class="form-control" name="title">
				@if ($errors->has('title'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
           		@endif
			</div>
			<div class="form-group">
				<label for="description">{{ __ ('common.description') }}</label>
				<textarea style="min-height: 100px" spellcheck="false" name="description" class="form-control" cols="30" rows="5">{{ old ('description') }}</textarea>
				@if ($errors->has('description'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
           		@endif
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-9">
						<label for="price">{{ __ ('common.priceInCurrency') }}</label>
						<input value="{{ old ('price') }}" type="text" class="form-control" name="price">
						@if ($errors->has('price'))
		                    <span class="invalid-feedback d-block" role="alert">
		                        <strong>{{ $errors->first('price') }}</strong>
		                    </span>
		           		@endif
					</div>

					<div class="col-md-3">
						<label for="currency">{{ __ ('common.currency') }}</label>
						<input value="{{ old ('currency') }}" id="currencies" type="text" class="form-control" name="currency">
						@if ($errors->has('currency'))
		                    <span class="invalid-feedback d-block" role="alert">
		                        <strong>{{ $errors->first('currency') }}</strong>
		                    </span>
		           		@endif
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="city">{{ __ ('common.city') }}</label>
				<input value="{{ old ('city') }}" id="input-query"" type="text" class="form-control" name="city">
				@if ($errors->has('city'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
           		@endif
			</div>

			<div class="form-group">
				<label for="captcha">{{ __ ('auth.captcha') }}</label>
				<div class="p-3">@captcha</div>
				<input type="text" class="form-control" name="captcha">
				@if ($errors->has('captcha'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('captcha') }}</strong>
                    </span>
           		@endif
			</div>

			<p>{{ __ ('ads.uploadAble') }}</p>

			<input type="submit" class="mb-5 btn btn-primary" value="{{ __ ('common.submit') }}">
		</form>
	</div>
@endsection