@extends ('layouts.page')
@section ('title', __('common.comedit'))

@section ('content')
<div class="row">
	<div class="col-md-8 offset-md-2">
		<div class="display-5">{{ __('common.comedit') }}</div>
		<form action="{{ route('comment.edit', ['id' => $id, 'com' => $com]) }}" class="form" method="post">
		@csrf
			<div>
				<a href="{{ route('ad', ['id' => $id]) }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> {{ __('common.back') }}</a>

				<button class="btn btn-success float-right">{{ __('common.submit') }} <i class="fa fa-save"></i></button>
			</div>

			<div class="row">
				<div class="col-md-6 col-sm-12">
					<label for="captcha">{{ __('auth.captcha') }}</label>
					<input type="text" name="captcha" class="form-control">
					@if ($errors->has('captcha'))
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $errors->first('captcha') }}</strong>
                        </span>
               		@endif
				</div>
				<div class="col-md-6 col-sm-12 p-3">
					@captcha
				</div>
			</div>

			<div class="form-group">
				<p class="fs-15">
					<span>{{ __('common.attitude') }}</span>
					@if ($errors->has('attitude'))
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $errors->first('attitude') }}</strong>
                        </span>
               		@endif
				</p>
				
				  <fieldset>
				    <label for="attitude-1"><i class="text-success fa fa-thumbs-o-up"></i></label>
				    <input type="radio" name="attitude" id="attitude-1" value="1">
				    <label for="attitude-2"><i class="text-danger fa fa-thumbs-o-down"></i></label>
				    <input type="radio" name="attitude" id="attitude-2" value="0">
				  </fieldset>
			</div>

			<div class="form-group mt-3">
				<label for="captcha">{{ __('common.comtext') }}:</label>
				@if ($errors->has('message'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('message') }}</strong>
                    </span>
           		@endif
				<textarea name="message" rows="10" spellcheck="false" class="p-1 form-control">{{ $comment->message }}</textarea>
			</div>
		</form>
	</div>
</div>
@endsection