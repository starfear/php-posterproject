@php
	$r = \Request::route();
	if (!$r)
		$route = 'error';
	else
		$route = $r->getName();
@endphp

<div class="qnavbar">
	<a href="{{ route('welcome') }}" class="qnavbar-brand"><i class="fa fa-bullhorn" aria-hidden="true"></i> {{ __('common.sitename') }}</a>
	<ul class="qnavbar-items">
		<li>
			<a href="{{ route('welcome') }}" class="{{ $route == "welcome" ? 'active' : '' }}">
				<i class="fa fa-home"></i>
				<span class="d-none d-sm-inline">{{ __('common.main') }}</span>
			</a>
		</li>
		<li>
			<a href="{{ route('ads') }}" class="{{ $route == "ads" ? 'active' : '' }}">
				<i class="fa fa-podcast text-warning"></i>
				<span class="d-none d-sm-inline">{{ __('common.ads') }}</span>
			</a>
		</li>
		<li>
			<a href="" class="{{ $route == "about" ? 'active' : '' }}">
				<i class="fa fa-address-book"></i>
				<span class="d-none d-sm-inline">{{ __('common.about') }}</span>
			</a>
		</li>
	</ul>

	<ul class="qnavbar-items float-right">
		@auth
			<li>
				<a href="{{ route('user.myprofile') }}"><i class="fa fa-user"></i> <span class="d-none d-sm-inline">{{ \Auth::user()->name }}</span></a>
			</li>
			<li>
				<form id="logoutform" action="{{ route('logout') }}" class="d-inline" method="POST">
				@csrf
				<a class="pointer" onclick="document.getElementById('logoutform').submit()"><i class="fa fa-sign-out"></i></a>
				</form>
			</li>
		@else
			<li>
				<a href="/login">
					<i class="fa fa-user-o" aria-hidden="true"></i> <span class="d-none d-sm-inline">{{__('common.login')}}</span>
				</a>
			</li>
			<li>
				<a href="/register">
					<i class="fa fa-user-plus" aria-hidden="true"></i> <span class="d-none d-sm-inline">{{__('common.register')}}</span>
				</a>
			</li>
		@endif
	</ul>
</div>