<script type="text/javascript">
	$ (document).ready (() => {
		$ ('#i-have-read').click ( e => {
			setCookie("ihaveread", "true", 365);
			$ ('#policyf-main').remove ();
		})
	})
</script>

<div class="policyf" id="policyf-main">
	<div class="policyf-inside">
		<div class="row pb-5">
			<div class="offset-md-1 col-md-9 col-sm-12 col-xs-12">
				<div class="fs-18">{{ __ ('messages.policyfTitle') }}</div>
				<div class="fs-15">
					{{ __ ('messages.policyfContent') }}
					<a href="">{{ __ ('messages.policyfLink1') }}</a>, 
					<a href="">{{ __ ('messages.policyfLink2') }}</a>, 
					<a href="">{{ __ ('messages.policyfLink3') }}</a>.
				</div>
			</div>
		</div>
		<button id="i-have-read" class="btn btn-primary">{{ __ ('common.got') }}</button>
	</div>
</div>