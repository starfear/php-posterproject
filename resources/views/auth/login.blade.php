@extends ('layouts.page')
@section ('title', __('common.login'))

@section ('content')
	<div class="row justify-content-center">
		<div class="col-md-8">
			<form action="{{ route('login') }}" class="form" method="POST">
				@csrf

				<div class="display-4">{{ __('common.login') }}</div>
				<div class="fs-15">{{ __('auth.goRegister') }} <a href="/register">{{ __('common.register') }}</a></div>

				<div class="form-group">
					<div class="pb-3">
						<div class="">
							<label for="name">{{ __('auth.email') }}</label>
							<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">
							@if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
						</div>
						<div class="">
							<label for="password">{{ __('auth.password') }}</label>
							<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}">
							@if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="captcha">{{ __('auth.captcha') }}</label>
					<div class="p-3">@captcha</div>
					<input type="text" id="captcha" name="captcha" class="form-control col-6{{ $errors->has('captcha') ? ' is-invalid' : '' }}">
					@if ($errors->has('captcha'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('captcha') }}</strong>
                                </span>
                            @endif
				</div>

				<input type="submit" class="btn btn-primary" value="{{ __('common.submit') }}">

				{{-- 
					email, password, captcha
				--}}
			</form>
		</div>
	</div>
@endsection