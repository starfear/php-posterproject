@extends ('layouts.page')
@section ('title', __('common.register'))

@section ('content')
	<div class="row justify-content-center">
		<div class="col-md-8">
			<form action="{{ route('register') }}" class="form" method="POST">
				@csrf

				<div class="display-4">{{ __('common.register') }}</div>
				<div class="fs-15">{{ __('auth.goLogin') }} <a href="/login">{{ __('common.login') }}</a></div>


				<div class="form-group">
					<div class="row pb-3">
						<div class="col-md-6 col-sm-12">
							<label for="firstname">{{ __('common.firstname') }}</label>
							<input type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}">
							@if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
						</div>
						<div class="col-md-6 col-sm-12">
							<label for="lastname">{{ __('common.lastname') }}</label>
							<input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}">
							@if ($errors->has('lastname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </span>
                            @endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row pb-3">
						<div class="col-md-6 col-sm-12">
							<label for="name">{{ __('auth.name') }}</label>
							<input type="text" pattern="[A-Za-z0-9]+" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}">
							@if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
						</div>
						<div class="col-md-6 col-sm-12">
							<label for="email">{{ __('auth.email') }}</label>
							<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">
							@if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row pb-3">
						<div class="col-md-6 col-sm-12">
							<label for="password">{{ __('auth.password') }}</label>
							<input pattern="[A-Za-z0-9]+" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
							@if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
						</div>
						<div class="col-md-6 col-sm-12">
							<label for="password-confirm">{{ __('auth.password-confirm') }}</label>
							<input type="password" class="form-control" name="password_confirmation">
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6 col-sm-12">
						<label for="captcha">{{ __('auth.captcha') }}</label>
						<div class="p-3">@captcha</div>
						<input type="text" id="captcha" name="captcha" class="form-control col-6{{ $errors->has('captcha') ? ' is-invalid' : '' }}">
						@if ($errors->has('captcha'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('captcha') }}</strong>
	                        </span>
	                    @endif
                    </div>
                    <div class="col-md-6">
						<label for="gender">{{ __('common.gender') }}</label>
						@if ($errors->has('gender'))
	                        <span class="invalid-feedback d-inline-block" role="alert">
	                            <strong>{{ $errors->first('gender') }}</strong>
	                        </span>
	                    @endif
						<fieldset>
							<div>
							    <label for="gender-1">{{ __('common.gender-1') }}</label>
							    <input type="radio" name="gender" id="gender-1" value="1">
							</div>
							<div>
							    <label for="gender-2">{{ __('common.gender-0') }}</label>
							    <input type="radio" name="gender" id="gender-2" value="0">
						    </div>
					  	</fieldset>
						@if ($errors->has('captcha'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('captcha') }}</strong>
	                        </span>
	                    @endif
                    </div>
				</div>

				<input type="submit" class="btn btn-primary" value="{{ __('common.submit') }}">

				{{-- 
					name, email, password, password-confirm
				--}}
			</form>
		</div>
	</div>
@endsection