@extends ('layouts.page')
@section ('title', __('common.myprofile'))

@section ('content')
	<div class="row">
		<div class="col-md-3 col-sm-12 mb-2">
			<img class="rounded-circle m-3 d-block" width="128" src="{{ $user->photo }}" alt="{{ $user->photo }}">
			<a href="{{ route('avatar') }}" class="btn btn-outline-info">{{ __('user.changeAvatar') }}</a>
		</div>
		<div class="col-md-9">
			<table class="table table-dark">
				<tbody>
					@foreach ($params as $k => $v)
						<tr>
							<td class="col-4">{{ $k }}</td>
							<td class="col-7">{{ $v }}</td>
							<td>
								<a onclick="">
									<i class="float-right text-white fa fa-edit"></i>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="row p-1">
		<div class="col-md-4 col-sm-12">
			<div class="fs-20">{{ __('common.ads') }}</div>
			<ul>
				@foreach ($user->ads()->orderBy('id', 'desc')->get() as $ad)
					<li><a href="{{ route('ad', ['id' => $ad->id]) }}">{{ $ad->title }}</a></li>
				@endforeach
			</ul>
		</div>
		<div class="col-md-8 col-sm-12">
			<div class="fs-20">{{ __('common.actions') }}</div>
			<a href="" class="btn btn-primary"><i class="fa fa-key"></i> {{ __('user.changePassword') }}</a>
			<a href="" class="btn btn-warning">{{ __('user.changeEmail') }}</a>
		</div>
	</div>
@endsection