@extends ('layouts.page')
@section ('title', __('errors.404-title'))

@section ('content')
	<div class="alert alert-danger">
		<div class="alert-heading">{{ __('errors.404-title') }}</div>
		<div class="alert-body">
			<p>{{ __('errors.404-desc') }}</p>
			<p><a class="btn btn-danger" href="/"><i class="fa fa-home"></i> {{ __('common.main') }}</a></p>
		</div>
	</div>
@endsection