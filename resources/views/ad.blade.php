@php
	$comments = $ad->comments()->orderBy('id', 'desc')->get();
	$count = count($comments);
	$reputation = $ad->user->reputation;
	$bad = $reputation < 0;
	$user = Auth::user ();
@endphp

@extends ('layouts.page')
@section ('title', $ad->title)

@section ('content')
	<a href="{{ route('ads') }}" class="btn btn-warning fs-15"><i class="fa fa-arrow-left"></i> {{ __('common.backToList') }}</a>

	@if (Auth::check () && $ad->user->id == Auth::user()->id)
	<div class="pt-3 pb-1">
		{{-- <span class="display-5">{{ __('common.actions') }}:</span> --}}
		<div class="d-inline-block">
			<a href="{{ route ('ad.edit', $ad->id) }}" class="btn btn-success"><i class="fa fa-edit"></i> {{ __('common.edit') }}</a>
			<form action="{{ route('ad.delete') }}" method="POST" class="d-inline-block">
				@csrf
				<input type="hidden" name="id" value="{{ $ad->id }}">
				<button class="btn btn-danger"><i class="fa fa-trash"></i> {{ __('common.delete') }}</button>
			</form>
		</div>
	</div>
	@endif

	<div class="display-5">{{ $ad->title }}</div>
	<div class="row">
		<div class="col-md-5">
			<div class="m-3">
				<span class="sn mr-4">{{ $ad->user->fullname }} <span class="sb text-primary">{{ __('common.user') }}</span></span>
				<span class="sn mr-4 {{ $bad ? 'text-danger' : '' }}">
					@if ($bad)
						<i class="fa fa-warning"> </i>
					@endif
					{{ $reputation }} 
					<span class="sb text-primary">{{ __('common.reputation') }}</span>
				</span>
			</div>
	
			<div class="m-3">
				<span class="sn mr-4">{{ $ad->currency }} <span class="sb text-success">{{ __('common.currency') }}</span></span>

				<span class="sn mr-4">{{ $ad->price }} <span class="sb text-success">{{ __('common.price') }}</span></span>
			</div>

			<div class="m-3 mb-5">
				<span class="sn mr-4">{{ $ad->city->country }} <span class="sb text-danger">{{ __('common.country') }}</span></span>
				
				<span class="sn mr-4">{{ $ad->city->name }} <span class="sb text-danger">{{ __('common.town') }}</span></span>

				<span class="sn mr-4">{{ $ad->updated_at }} <span class="sb text-danger">{{ __('common.creation_date') }}</span></span>
				
				@if ($bad)
				<div class="alert alert-danger">
					<div class="alert-heading">{{ __('ads.badRep') }}</div>
					<div class="alert-body">
						<p class="text-danger">
							{{ __('ads.badRepDesc') }} 
							<a href="{{ route('wiki', 'badrep') }}">
								{{ __('ads.badRepLink') }}
							</a>
						</p>
					</div>
				</div>
				@endif
			</div>
			<a href="" class="btn btn-{{ $bad ? 'danger' : 'primary' }} dalign-bottom"><i class="fa fa-shopping-cart"></i> {{ __('ads.contactWith') }}</a>
		</div>
		
		<div class="col-md-4">
			<p class="pre">{{ $ad->description }}</p>
		</div>

		<div class="col-md-3 col-sm-12 p-5">
			<div id="slider-image" class="w-100">
				@foreach ($ad -> images as $image)
					<img src="{{ $image -> url }}" alt="{{ $image -> alt }}">
				@endforeach
			</div>
		</div>
	</div>

	<script src="/js/s-ads.js"></script>

	{{-- comments --}}
	@if ($count)
		<div>
			<div class="fs-20">{{ __('ads.authorReviews') }} <span class="text-primary">{{ $count }}</span></div>
			@foreach ($comments as $comment)
				@php
					$attitude = $comment->attitude ? 'fa-thumbs-o-up text-success' : 'fa-thumbs-o-down text-danger';
				@endphp
				<div class="ad-review">
					<div class="row">
						<div class="col-md-1">
							<img src="{{ $comment->user->photo }}" height="80" class="img" alt="{{ $comment->user->fullname }}">
						</div>
						<div class="col-md-8">
							<div class="fs-14">{{ $comment->user->fullname }} <i class="fa {{ $attitude}}"></i></div>

							<p class="fs-12 comment-description hm-90 w-100 pre" id="ad{{ $comment->id }}">{{ $comment->message }}</p>
							
							<script type="text/javascript">
								ads.save ( {{$comment->id}}, '{{ __('ads.showFull') }}' )
							</script>

						</div>
						<div class="col-md-3">
							<div class="float-right">
								<p>{{ $comment->updated_at }}</p>
								@if ($user && $comment->user->id == $user->id)
									@if ($user->can('comments.delete'))
										<form action="{{ route('comment.delete', $ad->id) }}" method="POST" class="form">
											@csrf
											<input type="hidden" name="comment_id" value="{{ $comment->id }}">
											<button class="mr-1 btn btn-outline-danger float-right"><i class="fa fa-trash"></i></button>
										</form>
									@endif

									@if ($user->can('comments.delete'))
											<a href="{{ route('comment.editform', ['id' => $ad->id, 'com' => $comment->id]) }}" class="mr-1 btn btn-outline-primary float-right"><i class="fa fa-edit"></i></a>
										</form>
									@endif
								@endif
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif

	<div>
		<div class="fs-20">{{ __('ads.writeReview') }}</div>
			<div class="ad-review">
				@if (Auth::check() && Auth::user()->can('comments.add'))
					<div class="mb-3 fs-13 text-secondary">{{ __('ads.writingAs', ['name' => $user->name]) }}</div>

					<form action="{{ route('comment.new', $ad->id) }}" method="POST" class="form">
						@csrf
						<div class="form-group">
							<textarea rows="7" spellcheck="false" type="text" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message"></textarea>
							@if ($errors->has('message'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
						</div>

						<div class="form-group">
							<label for="captcha">{{ __('auth.captcha') }}</label>
							<div class="mt-3 mb-3">@captcha</div>
							<input spellcheck="false" type="text" class="form-control{{ $errors->has('captcha') ? ' is-invalid' : '' }}" name="captcha">
							@if ($errors->has('captcha'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('captcha') }}</strong>
                                </span>
                            @endif
						</div>

						<div class="form-group">
							<p class="fs-15">
								<span>{{ __('common.attitude') }}</span>
								@if ($errors->has('attitude'))
	                                <span class="invalid-feedback d-block" role="alert">
	                                    <strong>{{ $errors->first('attitude') }}</strong>
	                                </span>
                           		@endif
							</p>
							
							  <fieldset>
							    <label for="attitude-1"><i class="text-success fa fa-thumbs-o-up"></i></label>
							    <input type="radio" name="attitude" id="attitude-1" value="1">
							    <label for="attitude-2"><i class="text-danger fa fa-thumbs-o-down"></i></label>
							    <input type="radio" name="attitude" id="attitude-2" value="0">
							  </fieldset>
						</div>

						<input type="submit" class="btn btn-secondary mt-1" value="{{ __('common.submit') }}">
					</form>
				@else
					@if ($user && $user->can('comments.add'))
						<div class="fs-13 text-center text-secondary"><i class="fa fa-user-o"></i> {{ __('common.authOnly') }}</div>
						<div class="text-center">
							<a href="/register" class="fs-13 m-1 btn btn-outline-secondary">{{ __('common.register') }}</a>
							<a href="/login" class="fs-13 m-1 btn btn-outline-secondary">{{ __('common.login') }}</a>
					</div>
					@else
						<div class="fs-13 text-center text-secondary"><i class="fa fa-lock"></i> {{ __('permissions.comments.add') }}</div>
					@endif
				@endif
			</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
		  $('#slider-image').slick();
		});
	</script>
@endsection