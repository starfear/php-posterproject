@extends ('layouts.page')
@section ('title', __('common.ads'))

@section ('content')
	<div class="offset-md-1 col-md-10">
		{{-- section search --}}
		<form action="" class="form">
			<div class="form-group">
				<div class="row">
					<div class="col-md-9 pb-1">
						<div class="ui-widget">
							<input name="query" id="input-query" type="text" placeholder="{{ __('ads.search') }}" class="form-control">
						</div>
					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<button class="btn btn-dark w-100 p-2">
							<i class="fa fa-search"></i>
							{{ __('common.search') }}
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>

	<script src="/js/autocomplete_cities.js"></script>
	{{-- endsection search --}}

	{{-- section list --}}
	<div class="row pl-4 pr-4">
		<div class="col-md-3 offset-md-1 p-1">
			<div class="bg-black p-1 w-100 rounded p-2">
				<span>{{ __('common.options') }}</span>
				<a class="text-white float-right" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter"> <i class="fa fa-bars"></i></a>
				<div class="collapse show pt-2" id="collapseFilter">
					<div id="listTypeC">
						@php
							$list_type = 'list';
							$class = "border-success text-success";
						@endphp
					{{-- border-success text-success --}}
						<span>{{ __('ads.listtype') }}</span>

						{{-- Type: list --}}
						<div class="btn {{ $list_type == 'list' ? $class : '' }} hover-primary"><i class="fa fa-list" aria-hidden="true"></i></div>

						{{-- Type: table --}}
						<div class="btn {{ $list_type == 'table' ? $class : '' }} hover-primary"><i class="fa fa-table" aria-hidden="true"></i></div>

						{{-- Type: thl --}}
						<div class="btn {{ $list_type == 'thl' ? $class : '' }} hover-primary"><i class="fa fa-th-large" aria-hidden="true"></i></div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7 p-1">
			{{-- Type: list --}}
			<a href="{{ route ('ad.create') }}" class="btn btn-success mb-1 p-2 w-100"><i class="fa fa-plus"></i> {{ __('common.createAd') }}</a>
			@foreach ($ads as $ad)
				<div class="bg-white p-2 rounded ad">
					<div class="ad-title p-1">
						<span class="ad-title-name">{{ $ad->title }}</span>
						<span class="ad-title-author pr-3 text-gray">{{ $ad->user->name }}</span>
						<span class="ad-title-date text-silver">{{ $ad->updated_at }}</span>
					</div>
					<div class="p-1 bg-secondary rounded"></div>
					<div class="p-3">
						<div>
							<span class="sn mr-5">{{ $ad->city->name }} <span class="sb text-success">{{ __('common.town') }}</span></span>

							<span class="sn mr-5">{{ $ad->city->country }} <span class="sb text-success">{{ __('common.country') }}</span></span>

							<span class="sn mr-5">{{ $ad->currency }} <span class="sb text-success">{{ __('common.currency') }}</span></span>

							<span class="sn mr-5">{{ $ad->price }} <span class="sb text-success">{{ __('common.price') }}</span></span>

						</div>
						<div class="ad-desc">
							<div class="ad-desc-content">
								{{ $ad->description }}
							</div>
						</div>

						{{-- Images --}}
						<div class="p-3">
							@foreach ($ad -> images as $image)
								<a href="{{ $image -> url }}"><img src="{{ $image -> url }}" alt="{{ $image -> alt }}" class="mini-image" class="zoom-img"></a>
							@endforeach
						</div>

						<div class="pt-1">
							<a href="{{ route('ad', $ad->id) }}" class="btn btn-primary">{{ __('common.more') }}</a>
							<div class="float-right">
								<a href="" class="btn btn-outline-warning">
									<i class="fa fa-bookmark"></i>
									<span class="d-none d-sm-inline-block">{{ __('common.bookmark') }}</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			{{ $ads->links('components.pagination') }}
		</div>
	</div>
	{{-- endsection list --}}
@endsection