@extends ('layouts.page')
@section ('title', __('welcome.title') . ' ' . __('common.sitename'))

@section ('content')
    <div class="pan-welcome p-3">
        <div class="text-center">
            <div class="display-5">{{ __('welcome.title') }}</div>
            <div class="display-4">{{ __('common.sitename') }}</div>
            <div class="p-3">{{ __('welcome.description') }}</div>
        </div>
        @auth
            <div class="text-center">
                <a href="{{ route('ads') }}" class="btn btn-warning">{{ __('welcome.toAds') }}</a>
            </div>
        @else
            <div class="row align-center">
                <div class="col-md-3 offset-md-3 p-1">
                   <a href="{{ route('register') }}" class="btn btn-primary w-100">
                        {{ __('common.register') }}
                    </a> 
                </div>
                <div class="col-md-3 p-1">
                    <a href="{{ route('login') }}" class="btn btn-success w-100">
                        {{ __('common.login') }}
                    </a> 
                </div>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-3 offset-md-2 p-1">{{ __('welcome.desc1') }}</div>
        <div class="col-md-3 p-1">{{ __('welcome.desc2') }}</div>
        <div class="col-md-3 p-1">{{ __('welcome.desc3') }}</div>
    </div>
@endsection