@extends ('layouts.page')
@section ('title', $title)

@section ('content')
	<div class="row">
		<div class="col-md-6 offset-md-3 alert alert-{{ $type }}">
			{{ $message }}
		</div>
	</div>
@endsection