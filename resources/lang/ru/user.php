<?php

return [
	'changeAvatar' 	=> 'Смена аватара',
	'labelAvatar'	=> 'Форматы: jpeg,png,jpg,gif,svg',
	'changePassword'=> 'Сменить пароль',
	'changeEmail'   => 'Сменить E-Mail',
	'deleteAccount' => 'Удалить аккаунт',
];