<?php

return [
	'name'				=> 'Логин',
	'email'				=> 'E-Mail',
	'password'			=> 'Пароль',
	'password-confirm'	=> 'Повторите пароль',
	'captcha'			=> 'Введите код с изображения',
	'goRegister'		=> 'Впервые на сайте?',
	'goLogin'			=> 'Уже зарегистрированы?',
];