<?php

return [
	'search' 		=> 'Введите ключевые слова для поиска объявления.',
	'listtype'		=> 'Тип списка:',
	'fulldesc'		=> 'Полностью',
	'contactWith'	=> 'Связаться с продавцом',
	'authorReviews'	=> 'Отзывы о продавце',
	'add'			=> 'Добавить',
	'addNew'		=> 'Добавление предложения',
	'edit'			=> 'Изменение предложения',
	'showFull'		=> 'Показать полностью',
	'writeComment'	=> 'Написать комментарий:',
	'writeReview'	=> 'Оставить отзыв:',
	'writingAs'		=> 'Вы зашли как :name',
	'badRep'		=> 'Отрицательная репутация',
	'badRepDesc'	=> 'У этого продавца отрицательная репутация! Подробнее: ',
	'badRepLink'	=> 'Отрицательная репутация.',
	'uploadAble'	=> 'Вы сможете загрузить изображения после создания объявления.',
	'uploadImage'	=> 'Загрузка изображения',
	'backToView' 	=> 'Вернутся к просмотру',



	'thereisnocurrencies' => 'Вы ввели неправильную валюту, попробуйте выбрать из списка или напишите ее код.',
	'thereisnocities'	  => 'Вы ввели неправильное название города, попробуйте выбрать из списка.',

	// 'thereisnocurrencies' => 'There is no such currencies, select from suggestions.',
	// 'thereisnocities'	  => 'There is no such cities, select from suggestions.',
];