<?php

return [
	'message.required'	=> 'Сообщнеие не может быть пустым.',
	'message.min'		=> 'Длина сообщения поля должна быть больше :min.',
	'message.max'		=> 'Длина сообщения поля должна быть меньше :max.',
	'attitude'			=> 'Укажите вашу оценку.',
	'captcha'			=> 'Введите код с картинки.',
	'name.regex'		=> 'Для имени можно использовать только латинские буквы и арабские цифры.',
	'firstname.min'		=> 'Длина имени должна быть больше :min',
	'firstname.max'		=> 'Длина имени должна быть больше :max',
	'lastname.min'		=> 'Длина имени должна быть больше :min',
	'lastname.max'		=> 'Длина имени должна быть больше :max',

	'numeric'			=> 'Значине поля должно быть числом.',

	'captcha'			=> 'Неверный код с изображения, повторите попытку.',

	'firstname.required' => 'Укажите Имя',
	'lastname.required' => 'Укажите Фамилию',
	'name.required' => 'Укажите Логин',
	'email.required' => 'Укажите E-Mail',
	'gender.required' => 'Укажите пол',
	'password.required' => 'Укажите пароль',

	'captcha.required' => 'Укажите код',
	'image.required' => 'Загрузите изображение',
	'image.image' => 'Неправильный формат',
	'image.max'	=> 'Вес изображения не должен быть больше :max KB',

	'image.dimensions' => 'Неверное разрешение. Изображение должно быть минимум :min_width на :min_height пикселей, максимум :max_width на :max_height пикселей.',


	'deleted'	=> 'Успешно удалено.',
	'created'	=> 'Успешно созданно.',
	'edited'	=> 'Успешно измененно.',

	'required'	=> 'Это поле обьязательно.',
	'min'		=> 'Длина имени должна быть больше :min',
	'max'		=> 'Длина имени должна быть меньше :max',


	'confirmImageDeletion' => 'Вы действительно хотите удалить изображение?',

	'policyfTitle'   => 'Предупреждение',
	'policyfContent' => 'Используя наш сайт вы подтверждаете что вы прочитали и поняли: ',
	'policyfLink1' 	 => 'Соглашение на использование личных данных',
	'policyfLink2' 	 => 'Соглашение на использование Cookies',
	'policyfLink3' 	 => 'Соглашение на использование продукта',

];