<?php

return [
	'title' => 'Добро пожаловать в',
	
	'description' => 'Большая, быстрая, надежная площадка для объявлений.',

	'toAds' => 'Перейти к объявлениям',

	'desc1' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus facilis repellat amet ipsum consequatur corrupti recusandae minima nobis libero et, excepturi commodi sapiente placeat odio corporis, reprehenderit nesciunt, quibusdam quo.',

	'desc2' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore rerum explicabo voluptatibus pariatur tenetur exercitationem sed accusantium, dolore asperiores possimus consectetur ipsa. Dolor, deleniti, ducimus? Velit praesentium tempore ullam mollitia.',

	'desc3' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci voluptatibus natus expedita placeat ullam, numquam consequuntur explicabo non, consectetur eaque quia deleniti aspernatur ducimus voluptate laborum alias corrupti eum provident.',
];