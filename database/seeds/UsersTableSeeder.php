<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create ([
        	'name' 		=> 'admin',
        	'email' 	=> 'admin@this.is',
            'firstname' => 'Admin',
            'lastname'  => 'Adminius',
            'ismale'    => 1,
        	'password'	=> Hash::make('lol123'),
        ]);

        $admin->assignRole ('user');
        $admin->assignRole ('moder');
        $admin->assignRole ('super-admin');
    }
}
