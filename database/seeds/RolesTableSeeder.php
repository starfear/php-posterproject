<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# perms {
	        # Permissions for actions with comments
	        $comments_read   = Permission::create (['name' => 'comments']);
	        $comments_add    = Permission::create (['name' => 'comments.add']);
	        $comments_delete = Permission::create (['name' => 'comments.delete']);
	        $comments_edit 	 = Permission::create (['name' => 'comments.edit']);
	    	# Ads for moders
	        $comments_delete_mod = Permission::create (['name' => 'comments.delete.mod']);
	        $comments_edit_mod 	 = Permission::create (['name' => 'comments.edit.mod']);
	        # groups
	        $comments 	= Array ($comments_read, $comments_add, $comments_delete, $comments_edit);
	        $comments_m = Array ($comments_delete_mod, $comments_edit_mod);

	    	# Permissions for actions with Ads
	        $ads_read   = Permission::create (['name' => 'ads']);
	        $ads_add 	= Permission::create (['name' => 'ads.add']);
	        $ads_delete = Permission::create (['name' => 'ads.delete']);
	        $ads_edit 	= Permission::create (['name' => 'ads.edit']);
	        # Ads for moders
	        $ads_delete_mod = Permission::create (['name' => 'ads.delete.mod']);
	        $ads_edit_mod   = Permission::create (['name' => 'ads.edit.mod']);
	        # groups
	        $ads   = Array ($ads_read, $ads_add, $ads_delete, $ads_edit);
	        $ads_m = Array ($ads_delete_mod, $ads_edit_mod);

	        # Permission lists
	        $perms_user  = Array ($comments, $ads);
	        $perms_moder = Array ($comments_m, $ads_m);
        # }



        # roles {
        	# user
	        $role_user = Role::create (['name' => 'user']);
	        $role_user->syncPermissions ($perms_user);

	        # moder
	        $role_moder = Role::create (['name' => 'moder']);
	        $role_moder->syncPermissions ($perms_moder);

	        # super-admin
	        $role_sadmin = Role::create (['name' => 'super-admin']);
	        $role_sadmin->syncPermissions (Permission::all());
        # }

	    # TODO: make Permission file format & parser
    }
}
