<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = file_get_contents('jsons/cities/2017.json', FILE_USE_INCLUDE_PATH);

        foreach (json_decode($cities, true) as $city) {
        	City::create ($city);
        }
    }
}
