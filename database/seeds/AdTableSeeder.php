<?php

use Illuminate\Database\Seeder;
use App\Ad;
use App\City;
use App\User;
use App\Currency;

class AdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $ad = Ad::create ([
            	'title' => "Title: $i",
            	'description' => "Text $i",
            	'currency' => Currency :: byCode ('usd') -> code,
            	'price'	=> $i*mt_rand(1,1000),
                'user_id' => 1,
                'city_id' => 1,
            ]);

            $ad -> uploadImage (1, 'https://pbs.twimg.com/profile_images/1057899591708753921/PSpUS-Hp_400x400.jpg', 'Google');
        }
    }
}
