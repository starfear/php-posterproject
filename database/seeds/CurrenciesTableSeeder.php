<?php

use Illuminate\Database\Seeder;
use App\Currency;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$arr = [
    		['code' => 'usd', 'fullname' => 'United States Dollar'],
    		['code' => 'rub', 'fullname' => 'Russian Ruble'],
    	];

    	foreach ($arr as $a) {
    		Currency :: create ($a);
    	}
    }
}
