<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    public $table = 'users';

    protected $guard_name = 'web';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'ismale', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /**
     * Relation with Ads
     *
     * @var function
     */
    public function ads ()
    {
        return $this->hasMany ('App\Ad');
    }

    /**
     * Relation with Comments
     *
     * @var function
     */
    public function comments()
    {
        return $this->hasMany ('App\Comment');
    }

    /**
     * Get all related reviews
     *
     * @return query
     */
    public function reviews()
    {
        $ads = $this->ads->pluck('id');

        $comments = Comment::whereIn('ad_id', $ads);
        return $comments;
    }

    /**
     * Get all related reviews collection
     *
     * @var collection
     */
    public function getReviewsAttribute()
    {
        return $this->reviews()->get();
    }

    /**
     * Get reputation of user
     *
     * @var collection
     */
    public function getReputationAttribute()
    {
        return $this->reviews()->where('attitude', 1)->count() - $this->reviews()->where('attitude', 0)->count();
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullnameAttribute()
    {
        return "{$this->firstname} {$this->lastname}";
    }

    /**
     * Get gender in string
     *
     * @return integer
     */
    public function getGenderAttribute()
    {
        return __("common.gender-{$this->ismale}");
    }

    /**
     * Get photo url
     *
     * @return integer
     */
    public function getPhotoAttribute()
    {
        return $this->image != null ? $this->image : '/public/img/default-user.png';
    }
}
