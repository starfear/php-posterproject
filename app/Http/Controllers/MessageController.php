<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Show alert with INFO type
     *
     * @return \Illuminate\Http\Response
     */
    public static function info ($title, $message)
    {
    	return view ('messages.alert', [
    		'title'		=> $title,
    		'message'	=> $message,
    		'type'		=> 'info',
    	]);
    }
}
