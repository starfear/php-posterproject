<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Comment;
use App\Ad;
use Auth;

class CommentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    protected $rules = [
        'message'   => ['required', 'string', 'min:20', 'max:2018'],
        'captcha'   => ['required', 'captcha'],
        'attitude'  => ['required', 'boolean'],
    ];

    protected $deletion = [
        'comment_id'   => ['required', 'numeric'],
    ];


    protected $messages = array();

	/**
    * Inserts new Comment and shows ad.
    *
    * @return string
    **/
    public function new ($id, Request $request)
    {
    	if (!Ad::saveFind($id))
    		abort (404);

    	$validator = $this->validator($request->all());
    	
    	if ($validator->fails()) {
        	return AdController::read($id, $validator);
    	}

    	$this->create ([
    		'user_id' 	=> Auth::user()->id,
    		'ad_id'		=> $id,
    		'attitude'	=> $request->get('attitude'),
    		'message'	=> $request->get('message'),
    	]);

        return AdController::read($id);
    }

    /**
     * Delete comment
     *
     * @return string
     */
    public function delete ($id, Request $request)
    {
        $validator = $this->validatorDelete($request->all());

        if ($validator->fails()) {
            abort (400);
            // return AdController::read($id);
        }

        if (!$comment = Comment::find($request->get('comment_id')))
            abort (404);
        

        if ($comment->user->id != Auth::user()->id)
            abort (401);

        $comment->delete();

        # return AdController::read($id); # legacy
        return redirect()->route('ad', $id);
    }

    /**
     * Edit comment
     *
     * @return string
     */
    public function edit ($id, $com, Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return $this::editform($id, $com, $validator->errors());
        }

        $c = Comment::find ($com);
        
        if (!$c || $c->user->id != Auth::user()->id)
            abort (404);

        $c->message  = $request->get ('message');
        $c->attitude = $request->get ('attitude');
        $c->save ();

        return redirect()->route('ad', $id);
    }

    /**
     * Show the edit form
     *
     * @return \Illuminate\Http\Response
     */
    public function editform ($id, $com, $errors = null)
    {
        if (!Ad::find($id))
            return AdController::read ($id, $valid->errors());

        $comment = Comment::find($com);
        if (!$comment)
            abort (404);

        $v = view ('comments.edit', [
            'id'        => $id,
            'com'       => $com,
            'comment'   => $comment,
        ]);

        if ($errors)
            $v->with ('errors', $errors);

        return $v;
    }

    /**
     * Get a validator for an incoming comment registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, $this->rules, [
            'message.required'  => __('messages.message.required'),
            'message.min'    => __('messages.message.min', ['min' => 20]),
            'message.max'   => __('messages.message.max', ['max' => 255]),
            'captcha.required'  => __('messages.captcha'),
            'attitude.required' => __('messages.attitude'),
        ]);
    }

    /**
     * Get a validator for an incoming comment deletion request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorDelete(array $data)
    {
        return Validator::make($data, $this->deletion);
    }

    /**
     * Get a validator for an incoming comment edition request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorEdit(array $data)
    {
        return Validator::make($data, $this->edition);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return Comment::create([
            'ad_id'   	=> $data['ad_id'],
            'user_id' 	=> $data['user_id'],
            'attitude'	=> $data['attitude'],
            'message' 	=> $data['message'],
        ]);
    }
}
