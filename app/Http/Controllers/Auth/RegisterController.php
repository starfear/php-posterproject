<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/ads';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'min:5', 'max:20'], #, 'regex:[A-Za-z0-9]'
            'firstname' => ['required', 'string', 'min:3', 'max:20'],
            'lastname' => ['required', 'string', 'min:3', 'max:20'],
            'gender' => ['required', 'boolean'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'captcha' => ['required', 'captcha'],
        ], [
            'name.regex' => __('messages.name.regex'),
            'name.required' => __('messages.name.required'),
            'firstname.required' => __('messages.firstname.required'),
            'lastname.required' => __('messages.lastname.required'),
            'email.required' => __('messages.email.required'),
            'gender.required' => __('messages.gender.required'),
            'password.required' => __('messages.password.required'),
            'captcha.required' => __('messages.captcha.required'),
            'firstname:min' => __('messages.firstname.min', ['min' => 3]),
            'firstname:max' => __('messages.firstname.max', ['max' => 20]),
            'lastname:min' => __('messages.lastname.min', ['min' => 3]),
            'lastname:max' => __('messages.lastname.max', ['max' => 20]),
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'ismale' => $data['gender'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ])->assignRole ('user');
    }
}
