<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    /**
     * Shows image form.
     *
     * @param Obejct $errors
     * @return Response
     */
    public function imageform($errors = null)
    {
        $user = Auth::user ();
        $view = view('profile.avatar');
        if ($errors) 
            $view->with ('errors', $errors);

        return $view;
    }

    /**
     * Validate avatar request.
     *
     * @param  Request  $request
     * @return void
     */
    protected function validateAvatar (Request $request)
    {
        # Min:
        $min_h = 100;
        $min_w = 100;
        
        # Max:
        $max_h = 1024;
        $max_w = 1024;

        # Validating
        $valid = Validator::make ($request->all(), [
            'image' => [
                'required',
                'image',
                'mimes: png,jpg,jpeg,gif,svg',
                'max: 2048',
                "dimensions: min_w=$min_w,min_h=$min_h,max_w=$max_w,max_h=$max_h",
            ],
            'captcha' => 'required|captcha',
        ], [
            'image.required' => __('messages.image.required'),
            'image.max' => __('messages.image.max', ['max' => '2048']),
            'image.image' => __('messages.image.image'),
            'image.dimensions'  => __('messages.image.dimensions', ['min_w' => $min_w, 'min_h' => $min_h, 'max_w' => $max_w, 'max_h' => $max_h]),
            'captcha.required' => __('messages.captcha.required'),
        ]);

        if ($valid->fails ())
            return $this->imageForm ($valid->errors());
    }

    /**
     * Update the avatar for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function updavatar (Request $request)
    {
        # UserController@validateAvatar (Request $request)
        $v = $this->validateAvatar ($request);
        if ($v)
            return $v;

        # User object
        $user = Auth::user ();

        # Image data
        $data = $request->file ('image');

        # Future path
        $path = "public/avatars/{$user->name}.jpg";
        $public_path = "/storage/avatars/{$user->name}.jpg";

        # Some magic
        $img = Image::make ($data);
        $img->resize (1024, 1024);

        # Putting
        Storage::put ($path, $img->encode ('jpg'));
        
        # User changes and saving
        $user->image = $public_path;
        $user->save ();

        # redirect
        return redirect ()->route ('user.myprofile');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Renders myprofile view
     *
     * @return string
     */
    public function myprofile(Array $errors = null)
    {
        $user = Auth::user();

        $params = array (
            __('common.firstname')  => $user->firstname,
            __('common.lastname')   => $user->lastname,
            __('common.fullname')   => $user->fullname,
            __('common.gender')     => $user->gender,
        );

    	$view = view('profile.myprofile')->with('user', $user)->with('params', $params);
        if ($errors)
            $view->with ('errors', $errors);

        return $view;
    }
}
