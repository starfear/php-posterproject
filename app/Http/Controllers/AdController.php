<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\EnsureEmailIsVerified;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Auth;
use App\Ad;
use App\Currency;
use App\City;
use App\MImage;

class AdController extends Controller
{
    protected $rulesNew = [
            'title'       => 'required|min:3|max:25',
            'description' => 'required|min:25|max:1024',
            'price'       => 'required|numeric',
            'currency'    => 'required',
            'city'        => 'required',
            'captcha'     => 'required|captcha',
        ];

    protected $rulesUploadImage = [
        'image' => [
            'required',
            'image',
            'mimes: png,jpg,jpeg,gif,svg',
            'max: 2048',
            "dimensions: min_w=250,min_h=250,max_w=1024,max_h=1024",
        ],
        'captcha' => 'required|captcha',
        ];

    protected $rulesDeleteImage = [
        'image_id' => ['required'],
        'ad_id' => ['required'],
        ];

    protected $messagesNew         = Array ();
    protected $messagesUploadImage = Array ();
    protected $messagesDeleteImage = Array ();

    /**
    * Shows ads list
    *
    * @return string
    */
    public function index ()
    {
    	$ads = Ad::orderBy('id', 'DESC')->paginate(10);
    	return view ('ads', [
    		'ads' => $ads,
    	]);
    }

    /**
    * Shows edit form
    *
    * @return string
    */
    public function edit ($id)
    {
        $ad = Ad :: where ('id', $id) -> first ();

        if (!$ad)
            return abort (404);

        return view ('ad.edit', [
            'ad'        => $ad,
            'cityname'  => $ad->city->name,
        ]);
    }

    /**
    * Edits alreaady exist
    *
    * @return string
    */
    public function pedit (Request $request)
    {
        $id = $request -> get ('id');

        if (! $id)
            return abort (404);

        $validator = $this->validatorCreate ($request);

        if ($validator -> fails ()) 
            return redirect()->back()->withInput()->withErrors($validator -> errors());
            // return $this -> create ($validator -> errors (), $request);

        $errors = Array ();

        if (! $currency = Currency :: byCode ($request -> get ('currency')))
            array_push ($errors, ['currency' => ['There is no such currencies. Please use currency code like suggestion.']]);

        if (! $city     = City :: byName ($request -> get ('city')))
            array_push ($errors, ['currency' => __ ('ads.thereisnocities')]);

        if (count ($errors) != 0)
            return redirect()->back()->withInput()->withErrors($errors);
            // return $this -> create ($validator -> errors (), $request);

        $ad = Ad :: where ('id', $id) -> first ();
        $user = Auth :: user ();

        if (!$ad)
            return abort (404);

        if ($ad -> user_id != $user -> id && ! $user -> hasPermissionTo ('ads.delete.mod'))
            return abort (403);

        $title       = $request -> get ('title');
        $description = $request -> get ('description');
        // $currency : Instance from Currency:Model [code = 'usd', fullname = 'United States Dollar']
        $price       = $request -> get ('price');
        // $city     : Instance from City:Model

        $ad->title = $title;
        $ad->description = $description;
        $ad->currency = $currency->code;
        $ad->price = $price;
        $ad->city_id = $city -> id;
        $ad->save ();

        return MessageController::info (__('common.success'), __('messages.edited'));

        // return view ('adnew');
    }

    /**
    * Shows create form
    *
    * @return string
    */
    public function create ()
    {
        $view = view ('ad.new');

        return $view;
    }

    /**
    * Adds new
    *
    * @return string
    */
    public function pcreate (Request $request)
    {
        $validator = $this->validatorCreate ($request);

        if ($validator -> fails ()) 
            return redirect()->back()->withInput()->withErrors($validator -> errors());
            // return $this -> create ($validator -> errors (), $request);

        $errors = Array ();

        if (! $currency = Currency :: byCode ($request -> get ('currency')))
            array_push ($errors, ['currency' => ['There is no such currencies. Please use currency code like suggestion.']]);

        if (! $city     = City :: byName ($request -> get ('city')))
            array_push ($errors, ['currency' => __ ('ads.thereisnocities')]);

        if (count ($errors) != 0)
            return redirect()->back()->withInput()->withErrors($errors);
            // return $this -> create ($validator -> errors (), $request);

        $title       = $request -> get ('title');
        $description = $request -> get ('description');
        // $currency : Instance from Currency:Model [code = 'usd', fullname = 'United States Dollar']
        $price       = $request -> get ('price');
        // $city     : Instance from City:Model

        Ad :: create ([
            'title'         => $title,
            'description'   => $description,
            'currency'      => $currency->code,
            'price'         => $price,
            'city_id'       => $city->id,
            'user_id'       => Auth :: user () -> id,
        ]);

        return MessageController::info (__('common.success'), __('messages.created'));

        // return view ('adnew');
    }

    /**
    * Show ad by given id
    *
    * @return string
    */
    public static function read ($id, $errors = false)
    {
        $ad = Ad::find($id);
        
        if (!$ad)
            abort(404);

        $view = view ('ad')->with('ad', $ad);

        if ($errors)
            $view->withErrors($errors);

        return $view;
    }

    /**
    * Delete ad by given id
    *
    * @return string
    */
    public static function delete (Request $request)
    {
        $val = Validator::make ($request->all(), [
            'id'    => 'required|integer',
        ]);

        if ($val->fails ())
            abort (400);

        $id = $request->get ('id');
        $ad = Ad::find ($id);

        if (!$ad)
            abort (404);

        if ($ad->user->id != Auth::user ()->id)
            abort (401);

        $ad->delete ();

        return MessageController::info (__('common.success'), __('messages.deleted'));
    }

    /**
    * Shows addImage form
    *
    * @return string
    */
    public function addImageForm ($id)
    {
        $user = Auth :: user ();
        $ad   = Ad :: find ($id);

        if (!$ad)
            return 404;
        
        if (!$user -> hasPermissionTo ('ads.edit.mod') && ($ad->user_id != $user -> id) || !$user -> hasPermissionTo ('ads.edit'))
            return 403;

        return view ('ad.uploadImage') -> with ('ad', $ad);
    }

    /**
    * Adds new
    *
    * @return string
    */
    public function addImagePost (Request $request)
    {
        $validator = $this->validatorUploadImage ($request);

        if ($validator -> fails ()) 
            return redirect()->back()->withInput()->withErrors($validator -> errors());

        $user = Auth :: user ();
        $ad   = Ad :: find ($request -> get ('id'));

        if (!$ad)
            abort (404);
        
        if (!$user -> hasPermissionTo ('ads.edit.mod') && ($ad->user_id != $user -> id) || !$user -> hasPermissionTo ('ads.edit'))
            abort (403);


        # Image data
        $data = $request->file ('image');
       
        # Image Instance
        $inst = MImage :: create (['user_id' => $user -> id, 'ad_id' => $ad -> id, 'alt' => 'await', 'url' => 'await']);

        # Future path
        $path = "public/ad-images/{$ad -> id}_{$inst -> id}.jpg";
        $public_path = "/storage/ad-images/{$ad -> id}_{$inst -> id}.jpg";

        # Some magic
        $img = Image :: make ($data);
        $img -> resize (1024, 768);

        # Putting
        Storage :: put ($path, $img -> encode ('jpg'));
        
        # saving
        $inst -> url = $public_path;
        $inst -> alt = "Uploaded by {$user -> fullname}";
        $inst -> save ();

        # redirect
        return redirect ()->route ('ad.edit', ['id' => $ad -> id]);
    }

    /**
    * Delete image from ad
    *
    * @return string
    */
    public function deleteImage (Request $request)
    {
        $user = Auth :: user ();
        $validator = $this -> validatorDeleteImage ($request);
        if ($validator -> fails ())
            abort (500);

        $image = MImage :: find ($request -> get ('image_id'));
        if (!$image)
            abort(404);
        // return 1;

        if (!$user -> hasPermissionTo ('ads.edit.mod') && ($image->user_id != $user -> id) || !$user -> hasPermissionTo ('ads.edit'))
            abort(403);

        $image -> delete ();
        return true;
    }


    public function __construct ()
    {
        $this->messagesNew = [
            'title.required' => __ ('messages.required'),
            'description.required' => __ ('messages.required'),
            'currency.required' => __ ('messages.required'),
            'price.required' => __ ('messages.required'),
            'price.numeric' => __ ('messages.numeric'),
            'city.required' => __ ('messages.required'),
            'captcha.required' => __ ('messages.required'),
            'captcha.captcha' => __ ('messages.captcha'),

            'title.min' => __ ('messages.min', ['min' => 3]),
            'title.max' => __ ('messages.max', ['max' => 25]),

            'description.min' => __ ('messages.min', ['min' => 25]),
            'description.max' => __ ('messages.max', ['max' => 1024]),
        ];

        $this->messagesUploadImage = [
            'id.required' => 'you will never see this and that\'s bad ;c ',
            'image.required' => __('messages.image.required'),
            'image.max' => __('messages.image.max', ['max' => '2048']),
            'image.image' => __('messages.image.image'),
            'image.dimensions'  => __('messages.image.dimensions', ['min_w' => 250, 'min_h' => 250, 'max_w' => 1024, 'max_h' => 1024]),
            'captcha.required' => __('messages.captcha.required'),
            'captcha.captcha' => __('messages.captcha'),
        ];

        $this->messagesDeleteImage = [
            'ad_id.required' => __ ('messages.required'),
            'image_id.required' => __ ('messages.required'),
        ];
    }

    private function validatorCreate (Request $request)
    {
        return Validator::make ($request->all(), $this->rulesNew, $this->messagesNew);
    }

    private function validatorUploadImage (Request $request)
    {
        return Validator::make ($request->all(), $this->rulesUploadImage, $this->messagesUploadImage);
    }

    private function validatorDeleteImage (Request $request)
    {
        return Validator::make ($request->all(), $this->rulesDeleteImage, $this->messagesDeleteImage);
    }
}
