<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    public static function getCities ()
    {
    	return City :: get ();
    }

    public static function getCityByName ($name)
    {
    	return City :: where ('name', $name) -> first ();
    }
}
