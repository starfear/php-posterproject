<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;

class CurrencyController extends Controller
{
    static public function getCurrencies ()
    {
    	return Currency :: get();
    }

    static public function getCurrencyByCode ($c)
    {
    	return Currency :: where('code', $c) -> first ();
    }
}
