<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function country ()
    {
    	return $this->belongsTo ('App\Country');
    }

    public function ads ()
    {
    	return $this->hasMany ('App\Ad');
    }

    public static function byName ($name)
    {
    	return static :: where ('name', $name) -> first ();
    }
}
