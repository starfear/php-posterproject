<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MImage extends Model
{
	public $fillable = ['user_id', 'ad_id', 'url', 'alt'];

    /**
     * Relation with Ad
     *
     * @var function
     */
    public function ad ()
    {
        return $this -> belongsTo ('App\Ad');
    }
}
