<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['ad_id', 'user_id', 'attitude', 'message'];

    /**
     * Relation with Ad
     *
     * @var function
     */
    public function ad()
    {
        return $this->hasOne('App\Ad');
    }

    /**
     * Relation with User
     *
     * @var function
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
