<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AImage extends Model
{
	/**
     * Relation with ad
     *
     * @var function
     */
    public function ad ()
    {
    	return $this->belongsTo ('App\Ad');
    }
}
