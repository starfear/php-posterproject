<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    public $fillable = ['title', 'description', 'currency', 'price', 'city_id', 'user_id'];
	public $table = 'ads';

	/**
     * Relation with cities
     *
     * @var function
     */
    public function city ()
    {
    	return $this->belongsTo ('App\City');
    }

    /**
     * Relation with currencies
     *
     * @var function
     */
    public function currency ()
    {
        return $this->belongsTo ('App\Currency');
    }

    /**
     * Relation with users
     *
     * @var function
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relation with [Ad Images]
     *
     * @var function
     */
    public function images()
    {
        return $this->hasMany('App\MImage');
    }

    /**
     * Relation with comments
     *
     * @var function
     */
    public function comments()
    {
        return $this -> hasMany ('App\Comment');
    }

    /**
     * Save Find
     *
     * @var function
     */
    public static function saveFind($id)
    {
        return self :: find ($id);
    }

    /**
     * Uplaod image for ad 
     *
     * @param $user_id, $url, $alt
     * @var function
     */
    public function uploadImage ($user_id, $url, $alt)
    {
        return MImage :: create ( ['user_id' => $user_id, 'ad_id' => $this -> id, 'url' => $url, 'alt' => $alt] );
    }
}
