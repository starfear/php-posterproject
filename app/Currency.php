<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public function ads ()
    {
    	return $this->hasMany ('App\Ad');
    }

    public static function byCode ($code)
    {
    	return static :: where ('code', $code) -> first ();
    }
}
