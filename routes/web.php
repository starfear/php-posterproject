<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Auth\Middleware\EnsureEmailIsVerified;
use App\Ad;
use App\User;
use App\Country;
use App\City;
use Gameizeazy\Belongs;

App::setlocale('ru');

Route::get('/', function () {
    return view('welcome');
})->name('welcome');


Route::get('/ads', 'AdController@index')->name('ads');
Route::get('/pr/{id}', 'AdController@read')->name('ad');

Route::group(['middleware' => 'permission:ads'], function () {
	Route::post('/p/delete', 'AdController@delete')
	->middleware('permission:ads.delete')
	->name('ad.delete');

	Route::group (['middleware' => 'permission:ads.add'], function () {
		Route::get('/p/new', 'AdController@create')->name('ad.create');
		Route::post('/p/new/p', 'AdController@pcreate')->name('ad.pcreate');
	});

	Route::group (['middleware' => 'permission:ads.edit'], function () {
		Route::get('/p/edit/{id}', 'AdController@edit')->name('ad.edit');
		Route::post('/p/edit', 'AdController@pedit')->name('ad.pedit');


		Route::get('/p/image/upload/{id}', 'AdController@addImageForm')->name('ad.uploadImageForm');
		Route::post('/p/image/up', 'AdController@addImagePost')->name('ad.uploadImagePost');

		Route::post('/p/image/delete', 'AdController@deleteImage')->name('ad.deleteImage');
	});
});


Route::group(['middleware' => 'permission:comments'], function () {

	# get
	Route::get('/comments/edit/{id}/{com}', 'CommentController@editForm')
	->middleware('permission:comments.edit')
	->name('comment.editform');

	# post
	Route::post('/pr/{id}', 'CommentController@new')
	->middleware('permission:comments.add')
	->name('comment.new');

	Route::post('/comments/edit/{id}/{com}', 'CommentController@edit')
	->middleware('permission:comments.edit')
	->name('comment.edit');

	Route::post('/comments/delete/', 'CommentController@delete')
	->middleware('permission:comments.delete')
	->name('comment.delete');
});

Route::get('/myprofile', 'UserController@myprofile')->name('user.myprofile');

# avatar
Route::get('/myprofile/avatar', 'UserController@imageform')->name('avatar');
Route::post('/myprofile/avatar', 'UserController@updavatar')->name('avatar.upd');



# wiki
Route::get('/wiki/{title}', function () {
	return 'There is nothing :(';
})->name('wiki');

Auth::routes(['verify' => true]);
