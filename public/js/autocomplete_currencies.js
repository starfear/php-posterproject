$(document).ready (() => {
  $.ajax ({
    url: '/json/currencies.json',

    success: body => {
      $ ("#currencies").autocomplete({
        source: body,
        delay: 500,
        minLength: 1
      });
    }
  })
});