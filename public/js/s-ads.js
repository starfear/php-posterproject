window.ads = {
	showFull: (e, id) => {
		let elem = $("#ad"+id)
		if (elem) 
			elem.css('max-height', 'inherit')
		$(e).remove()
	},

	save: (id, lang) => {
		let obj = $('#ad'+id)
		let height = obj.height()
		let q = '<button onclick="ads.showFull(this, '+id+')" class="mt-1 btn btn-outline-dark fs-10">'+lang+'</button>'
		if (height >= 90)
			document.write(q)
	}
}

