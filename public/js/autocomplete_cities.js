$(document).ready (() => {
  $.ajax ({
    url: '/json/russian-cities.json',

    success: body => {
      $( "#input-query" ).autocomplete({
        source: body,
        delay: 500,
        minLength: 3
      });
    }
  })
});